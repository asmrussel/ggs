
import spock.lang.Specification

import spock.lang.Unroll

class MySpecification2 extends Specification {

    def setupSpec() {}    // runs once -  before the first feature method

    def setup() {}        // runs before every feature method

    def "given expect"() {
        given:
        def a = 20
        expect:
        a == 20
    }

    def "when then"() {
        when:
        def a = 20
        then:
        a == 20
    }

    def "given when then"() {
        given:
        def b = 30
        when:
        def a = 20
        then:
        assert a == 20
        assert b != 30
    }

    def "HashMap accepts null key"() {
        given:
        def map = new HashMap()

        when:
        map.put(null, "elem")

        then:
        notThrown(NullPointerException)
    }

    def "HashMap accepts null key:fail"() {
        given:
        def map = new HashMap()

        when:
        map.put(null, "elem")

        then:
        thrown(NullPointerException)
    }

    def "Expect Blocks"() {
        expect:
        def a = 20
        Math.max(1, 2) == 2
        a != 20
    }

    def "cleanup Blocks"() {
        given:
        def file = new File("abc.txt")
        file.createNewFile()
        cleanup:
        file.delete()
    }

    @Unroll
    def "computing the maximum of #a and #b"() {
        expect:
        Math.max(a, b) == c

        where:
        a << [5, 3,6]
        b << [1, 9,9]
        c << [5, 9,2]
    }

    @Unroll
    def "computing the maximum of two numbers 2"() {
        expect:
        Math.max(a, b) == c

        where:
        a|b|c
        5|1|5
        3|9|9
        5|1|5
        3|9|9
        5|1|5
        3|9|9
        5|1|5
        3|9|9
        5|1|5
        3|9|9
        5|1|5
        3|9|9
        5|1|5
        3|9|9
    }

    @Unroll
    def "minimum of #a and #b is #c"() {
        expect:
        Math.min(a, b) == c

        where:
        a | b || c
        3 | 7 || 3
        5 | 4 || 4
        9 | 9 || 9
    }

    def cleanup() {}      // runs after every feature method

    def cleanupSpec() {} // runs once -  after the last feature method

}
