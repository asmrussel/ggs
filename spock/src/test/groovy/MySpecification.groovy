import spock.lang.Specification

class MySpecification extends Specification {
    // fields
    // fixture methods
    // feature methods
    // helper methods

    def "Jemmi Test"() {
        given:
        int a = 50
        int b = 2
        when:
        int c = a / b
        then:
        c == 26
    }

    def "Jemmi Test 123"() {
        given:
        int a = 50
        int b = 2
        expect:
        int c = a / b
        c == 25
    }
}
