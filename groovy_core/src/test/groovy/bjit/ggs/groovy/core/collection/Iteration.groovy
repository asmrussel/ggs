package bjit.ggs.groovy.core.collection

import org.junit.Test

import static org.junit.Assert.assertEquals

public class Iteration {
    @Test
    public void loop_each_range_with_print() {
        def numbers = (1..10)
        numbers.each { println it }
    }

    @Test
    public void loop_eachWithIndex_range_with_print() {
        def numbers = (1..10)
        numbers.eachWithIndex { number, i -> println "${i} = ${number}" }
    }

    @Test
    public void when_collect_it_returns_new_collection_with_closure() {
        def fruits = ["Banana", "Apple", "Grape", "Pear"]
        List upperCaseFruits = fruits.collect { it.toUpperCase() }

        assertEquals("BANANA", upperCaseFruits.get(0))
        assertEquals("APPLE", upperCaseFruits.get(1))
        assertEquals("GRAPE", upperCaseFruits.get(2))
        assertEquals("PEAR", upperCaseFruits.get(3))

        println upperCaseFruits
    }

    @Test
    public void when_collect_with_default_it_returns_new_collection_with_closure() {

        def initialFruits = ["ORANGE", "LEMON"]
        def fruits = ["Banana", "Apple", "Grape", "Berry"]
        def totalFruits = fruits.collect(initialFruits, { it.toUpperCase() })

        println totalFruits //[ORANGE, LEMON, BANANA, APPLE, GRAPE, BERRY]
    }
}
