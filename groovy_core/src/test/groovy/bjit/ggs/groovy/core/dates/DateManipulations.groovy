package bjit.ggs.groovy.core.dates

import org.junit.Test

import static org.junit.Assert.assertTrue

public class DateManipulations {
    @Test
    public void dateInitialization() {
        def date = new Date()
        println date // Sat Sep 26 19:22:50 EEST 2015
        def calendar = Calendar.instance
        println calendar // java.util.GregorianCalendar[time=1443284933986, ...
        assertTrue(true)
    }

    @Test
    public void dateParsing() {
        def date = Date.parse("dd.MM.yyy", '18.05.1988')
        println date // Wed May 18 00:00:00 EEST 1988

        def extendedDate = new Date().parse("dd.MM.yyy HH:mm:ss", '18.05.1988 12:15:00')
        println extendedDate // Wed May 18 12:15:00 EEST 1988
        assertTrue(true)
    }

    @Test
    public void dateFormatting() {
        def date = Date.parse("dd.MM.yyy", '18.05.1988')
        def formattedDate = date.format("dd/MM/yyy")
        println formattedDate // 18/05/1988
        assertTrue(true)
    }

    @Test
    public void dateArithmetic () {
        def date = Date.parse("dd.MM.yyy", '18.05.1988')
        def datePlus = date.clone()
        def dateMinus = date.clone()
        datePlus = datePlus + 5
        println datePlus // Mon May 23 00:00:00 EEST 1988
        datePlus = datePlus.plus(5)
        println datePlus // Sat May 28 00:00:00 EEST 1988
        dateMinus = dateMinus - 10
        println dateMinus // Sun May 08 00:00:00 EEST 1988
        dateMinus = dateMinus.minus(10)
        println dateMinus // Thu Apr 28 00:00:00 EEST 1988
        def dateInterval =  dateMinus..<datePlus
        println dateInterval // [Thu Apr 28 00:00:00 EEST 1988,.., Fri May 27 00:00:00 EEST 1988]

        assertTrue(true)
    }
}