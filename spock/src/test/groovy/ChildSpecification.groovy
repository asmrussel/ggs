import bjit.ggs.groovy.core.Person
import spock.lang.Shared
import spock.lang.Specification

class ChildSpecification extends ParentSpecification{
    //def person1 = new Person("a","b");
    //@Shared
    //def person2 = new Person("a","b");
    def cleanup() {
        println "Child cleanup"
    }      // runs after every feature method
    def cleanupSpec() {
        println "Child cleanupSpec"
    } // runs once -  after the last feature method
    def setupSpec() {
        println "Child setupSpec"
    }    // runs once -  before the first feature method
    def "given expect55"() {
        given:
        def a = 20
        expect:
        a == 20
    }
    def setup() {
        println "Child setup"
    }
    def "given expect556"() {
        given:
        def a = 20
        expect:
        a == 20
    }
}
