package bjit.ggs.groovy.core.collection

import bjit.ggs.java.core.Person;

import static org.junit.Assert.*
import org.junit.Test

public class Searching {
    @Test
    public void find_element_from_array_with_matching_value() {
        String[] names = ["Mary", "Becky", "Susan", "Tom"]
        String foundByName = names.find { it.equalsIgnoreCase("mary") }
        assertEquals("Mary", foundByName)
    }

    @Test
    public void find_element_from_map_with_matching_key() {
        def mymap = [name: "Mary", likes: "coding", id: 1, comment: "Fun to hang out"]
        def likesValue = mymap.find { it.key == "likes" }.value

        assertEquals("coding", likesValue)

        println likesValue
    }

    @Test
    public void find_element_from_list_with_matching_value() {
        List names = ["Mary", "Becky", "Susan", "Tom"]
        String foundByName = names.find { it.equalsIgnoreCase("mary") }
        assertEquals("Mary", foundByName)
    }

    @Test
    public void find_person_from_array_with_matching_firstName() {
        Person[] persons = [new Person("Mary", "Zheng"), new Person("Alex", "Zheng"), new Person("Allen", "Zheng")]
        Person found = persons.find { it.firstName == "Mary" }
        assertEquals("Mary", found.firstName)
        assertEquals("Zheng", found.lastName)
    }

    @Test
    public void find_evenNumbers_from_range_use_containAll_to_verify() {
        def numbers = (1..10)
        List evenNumbers = numbers.findAll { it % 2 == 0 }
        print evenNumbers

        List evenNumberUnderTen = [2, 4, 6, 8, 10]

        assertTrue(evenNumberUnderTen.containsAll(evenNumbers))
    }

    @Test
    public void findResults_from_list_with_matching_condition() {
        List names = ["Mary", "Becky", "Susan", "Tom"]
        String foundByName = names.findResult { if (it.startsWith("B")) it.toUpperCase() }
        assertEquals("BECKY", foundByName)
    }

    @Test
    public void grep_from_set_with_verify_with_equals() {
        Set names = new HashSet()
        names.add("Zhang")
        names.add("Mary")
        names.add("Mary")
        names.add("Zheng")
        Set foundNameWithH = names.grep { it.contains("h") }
        assertEquals(2, foundNameWithH.size())

        def hnames = ["Zhang", "Zheng"] as Set
        assertTrue(foundNameWithH.equals(hnames))

        println foundNameWithH
    }

    @Test
    public void grep_object_from_set() {
        Set mySet = [10, 20, 0, false, true, 'hello', [1, 2], [3, 4], null, "world"]
        println mySet.grep(Number)
        println mySet.grep(String)
        println mySet.grep(Boolean)
        println mySet.grep(Collection)
        println mySet.grep('hello')
    }
}
