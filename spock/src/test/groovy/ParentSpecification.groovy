import spock.lang.Specification

class ParentSpecification extends Specification {

    def cleanup() {
        println "Parent cleanup"
    }      // runs after every feature method
    def cleanupSpec() {
        println "Parent cleanupSpec"
    } // runs once -  after the last feature method
    def setupSpec() {
        println "Parent setupSpec"
    }    // runs once -  before the first feature method
    def setup() {
        println "Parent setup"
    }        // runs before every feature method
    def "given expect"() {
        given:
        def a = 20
        expect:
        a == 20
    }
    def "given expect3"() {
        given:
        def a = 20
        expect:
        a == 20
    }
}
