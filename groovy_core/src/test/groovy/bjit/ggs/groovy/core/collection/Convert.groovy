package bjit.ggs.groovy.core.collection

import org.junit.Test

import static org.junit.Assert.assertEquals

public class Convert {
    @Test
    public void convert_from_List_to_Set_duplicate_element_is_gone() {
        List names = ["Mary", "Becky", "Susan", "Tom", "Mary"]
        assertEquals(5, names.size())

        Set nameSet = names.toSet()
        assertEquals(4, nameSet.size())

        println nameSet
    }

    @Test
    public void convert_from_List_to_String() {
        List names = ["Mary", "Becky", "Susan", "Tom"]
        String nameValues = names.toListString()
        assertEquals("[Mary, Becky, Susan, Tom]", nameValues)

        println nameValues
    }

    @Test
    public void convert_from_List_to_Array() {
        List names = ["Mary", "Becky", "Susan", "Tom", "Mary"]
        assertEquals(5, names.size())

        String[] nameArray = names.toArray()
        assertEquals(5, nameArray.size())

        println nameArray
    }


    @Test
    public void covert_from_evenNumber_list_to_map_with_toSpreadMap() {
        def evenNumberList = ['key', 'value', 'name', 'mary', 'age', '40'] as Object[]

        def map = evenNumberList.toSpreadMap()

        assert 3 == map.size()
        assert 'value' == map.key
        assert 'mary' == map['name']
        assert '40' == map['age']
    }
}
