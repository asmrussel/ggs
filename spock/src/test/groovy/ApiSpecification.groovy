import org.apache.http.HttpEntity
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.utils.URIBuilder
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils
import spock.lang.Shared
import spock.lang.Specification

class ApiSpecification extends Specification{

    //@Shared
    CloseableHttpClient httpClient

    URIBuilder builder

    def setupSpec() {
        httpClient = HttpClients.createDefault();
    }

    def setup(){
        //httpClient = HttpClients.createDefault();
    }

    def "GET Api Test"(){
        given:
            URIBuilder builder = new URIBuilder("https://run.mocky.io/v3/32febd77-8067-4e14-aa6d-5884b872d969")
            HttpGet httpGet = new HttpGet(builder.build())
        when:
            CloseableHttpResponse httpResponse =  httpClient.execute(httpGet)
        then:
            String responseBody = EntityUtils.toString((HttpEntity) httpResponse.getEntity(), "UTF8")
            println httpResponse.getHeaders("Content-Type");
            httpResponse.getStatusLine().getStatusCode() == 200
            httpResponse.getHeaders("Content-Type")[0].getValue() == "application/xml; charset=UTF-8"
    }

}
