package bjit.ggs.groovy.core.string

import org.junit.Test

import static org.junit.Assert.assertTrue

public class SubStringManipulations {

    @Test
    public void substring() {
        def name = 'John' // John
        def log = "Exception on saving user with username:johntheripper"
        def username = log.substring(log.lastIndexOf(":") + 1, log.length())
        println username // johntheripper

        def usernameWithoutEndIndex = log.substring(log.lastIndexOf(":") + 1)
        println usernameWithoutEndIndex // johntheripper
        assertTrue(true)
    }

    @Test
    public void subsequence() {
        def log = "Exception on saving user with username:johntheripper"
        def username = log.subSequence(log.lastIndexOf(":") + 1, log.length())
        println username // johntheripper
        assertTrue(true)
    }

    @Test
    public void GetAt() {
        def text1 = "crazy fox jumps over lazy dog"
        println text1.getAt(0..(text1.length() - 5)) // crazy fox jumps over lazy

        def text2 = "keep calm and carry on"
        println text2.getAt(-1..5) // no yrrac dna mlac
        assertTrue(true)
    }

    @Test
    public void SubtractString() {
        def text1 = "Sorry, I need to separate from you"
        println text1 - " you" // Sorry, I need to separate from

        def text2 = "Minus string usage"
        println text2.minus(" usage") // Minus string

        assertTrue(true)
    }
}