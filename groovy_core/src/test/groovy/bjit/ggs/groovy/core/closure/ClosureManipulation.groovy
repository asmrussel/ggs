package bjit.ggs.groovy.core.closure

import org.junit.Test

import static org.junit.Assert.assertTrue

public class ClosureManipulation {
    @Test
    public void ClosureDeclaration() {
        def myClosure = { println "Hello World!" }
        myClosure.call() // Hello World!
        myClosure() // Hello World!
        assertTrue(true)
    }

    @Test
    public void ClosureParameters() {
        def squareWithImplicitParameter = { it * it }
        println squareWithImplicitParameter(4) // 16

        def sumWithExplicitTypes = { int a, int b -> return a + b }
        println sumWithExplicitTypes(11, 8) // 19

        def sumWithOneExplicitOneOptionalTypes = { int a, b -> return a + b }
        println sumWithOneExplicitOneOptionalTypes(20, 13) // 33

        def sumWithDefaultParameterValue = { a, b = 5 -> return a + b }
        println sumWithDefaultParameterValue(4)  // 9
        assertTrue(true)
    }

    @Test
    public void ClosureVarArgs() {
        def combine = { String... names ->
            names.join(',')
        }
        println combine('John', 'Doe', 'Betty') // John,Doe,Betty
        assertTrue(true)
    }

    @Test
    public void PassingClosure() {
        def funcClosure = { x, func ->
            func(x)
        }
        println funcClosure([1, 2, 3], { it.size() }) // 3
        assertTrue(true)
    }

    @Test
    public void ClosureComposition() {
        def sum = { a, b -> return a + b }
        def square = { it * it }
        def squareOfSum = square << sum
        println squareOfSum(2, 3) // 25
        assertTrue(true)
    }
}