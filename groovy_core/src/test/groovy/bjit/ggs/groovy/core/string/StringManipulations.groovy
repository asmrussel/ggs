package bjit.ggs.groovy.core.string

import org.junit.Test

import static org.junit.Assert.assertTrue

public class StringManipulations {
    @Test
    public void stringBasic() {
        def name = 'John' // John
        println name

        println 'The quick brown fox jumps over the lazy dog'.length() // 43
        println 'Non-Blocking IO'.indexOf("o") // 1
        println 'AngularJS Service vs Factory vs Provider'.substring(32) // Provider
        println 'C# is the best'.replace('C#', 'Java') // Java is the best
        println 'I am very angry'.toUpperCase() // I AM VERY ANGRY
        assertTrue(true)
    }

    @Test
    public void stringConcat() {
        def name = 'John'
        def surname = 'Doe'
        println 'Hello ' + name + ' ' + surname // Hello John Doe
        assertTrue(true)
    }

    @Test
    public void GString() {
        def name = 'John'
        def surname = 'Doe'

        println "Hello ${name} ${surname}" // Hello John Doe
        println 'Hellow ${name} ${surname}' // Hellow ${name} ${surname}
        assertTrue(true)
    }

    @Test
    public void Operators() {
        println 'I am very long sentence' - 'very long ' // I am sentence
        println 'I will ' + ' be very long sentence' // I will  be very long sentence
        println 'Ice ' * 2 + ' baby' // Ice Ice  baby

        assertTrue(true)
    }

    @Test
    public void MultiLinedString() {
        def multiLine = """
                Hi everyone, I will
                write lots of things here
                because I am not restricted with
                one line. I am capable of 
                multi lines
        """
        println multiLine

        assertTrue(true)
    }

    @Test
    public void Tokenize() {
        def text = 'Hello World'
        println text.tokenize() // [Hello, World]

        def textWithComma = 'Hello,World'
        println textWithComma.tokenize(',') // [Hello, World]

        def textWithTab = 'Hello    World'
        println textWithTab.tokenize()

        assertTrue(true)
    }
}