package bjit.ggs.groovy.core.collection

import bjit.ggs.java.core.Person
import org.junit.Test

import static org.junit.Assert.*

public class CollectionManipulation {
    @Test
    public void when_sort_array_then_modify_array_to_sorted_order() {
        String[] names = ["Mary", "Becky", "Susan", "Tom"]
        println names
        assertEquals(4, names.size())
        assertEquals("Mary", names[0])
        assertEquals("Becky", names[1])
        assertEquals("Susan", names[2])
        assertEquals("Tom", names[3])

        names.sort()
        assertEquals("Becky", names[0])
        assertEquals("Mary", names[1])
        assertEquals("Susan", names[2])
        assertEquals("Tom", names[3])

        println "Sorted name: "
        println names
    }

    @Test
    public void when_tosort_array_then_array_is_not_modified_but_new_sorted_array_is_returned() {
        String[] names = ["Mary", "Becky", "Susan", "Tom"]
        println names
        assertEquals(4, names.size())
        assertEquals("Mary", names[0])
        assertEquals("Becky", names[1])
        assertEquals("Susan", names[2])
        assertEquals("Tom", names[3])

        def sortedNames = names.toSorted()
        assertEquals("Becky", sortedNames[0])
        assertEquals("Mary", sortedNames[1])
        assertEquals("Susan", sortedNames[2])
        assertEquals("Tom", sortedNames[3])

        println "Sorted name: "
        println names
    }

    @Test
    public void when_sort_list_then_the_list_is_modified_in_sorted_order() {
        List names = ["Mary", "Becky", "Susan", "Tom"]
        assertEquals(4, names.size())
        assertEquals("Mary", names[0])
        assertEquals("Becky", names[1])
        assertEquals("Susan", names[2])
        assertEquals("Tom", names[3])

        names.sort()
        assertEquals("Becky", names[0])
        assertEquals("Mary", names[1])
        assertEquals("Susan", names[2])
        assertEquals("Tom", names[3])

        println "Sorted name: "
        names.each { print "${it}, " }
    }

    @Test
    public void when_sort_persons_the_persons_not_modified_but_new_sorted_persons_is_returned() {

        Person[] persons = [new Person("Mary", "Zheng"), new Person("Alex", "Zheng"), new Person("Allen", "Zheng")]
        assertEquals("Mary", persons[0].firstName)
        assertEquals("Alex", persons[1].firstName)
        assertEquals("Allen", persons[2].firstName)

        def orderByFirstName = persons.sort { it.firstName }

        assertEquals("Alex", orderByFirstName[0].firstName)
        assertEquals("Allen", orderByFirstName[1].firstName)
        assertEquals("Mary", orderByFirstName[2].firstName)

        orderByFirstName.each { println it }
    }

    @Test
    public void when_sort_set_the_set_not_modified_but_new_sorted_set_is_returned() {
        Set names = new HashSet()
        names.add("Zhang")
        names.add("Mary")
        names.add("Zheng")
        Set orderSet = names.sort()
        orderSet.eachWithIndex { name, i -> println "${i} = '${name}'" }
    }

    @Test
    public void when_groupby_list_with_matching_condition_it_returned_map_with_groupby_data() {
        List names = ["Mary", "Becky", "Susan", "Tom"]
        Map<Boolean, List> hasY = names.groupBy { it.contains("y") }

        assertEquals(2, hasY.size())
        assertTrue(hasY.keySet().contains(true))
        assertTrue(hasY.keySet().contains(false))
        assertEquals(2, hasY.get(true).size())
        assertTrue(hasY.get(true).contains("Becky"))
        assertTrue(hasY.get(true).contains("Mary"))
        assertEquals(2, hasY.get(false).size())
        assertTrue(hasY.get(false).contains("Susan"))
        assertTrue(hasY.get(false).contains("Tom"))

        println "\nGroup by name contains 'Y' or not: "
        hasY.each { println "${it.key}-${it.value}" }
    }

    @Test
    public void when_countby_list_with_matching_condition_it_returned_map_with_countby_count() {
        List names = ["Mary", "Becky", "Susan", "Tom"]
        Map hasY = names.countBy { it.contains("T") }
        assertEquals(2, hasY.size())
        assertTrue(hasY.keySet().contains(true))
        assertTrue(hasY.keySet().contains(false))
        assertEquals(1, hasY.get(true))
        assertEquals(3, hasY.get(false))

        println "\nCount by name contains 'T' or not: "
        hasY.each { println "${it.key}-${it.value}" }
    }

    @Test
    public void when_groupBy_it_returns_map_with_groupBy_data() {
        def numbers = (1..50)
        def sameUnitsDigits = numbers.groupBy { it % 10 }
        sameUnitsDigits.each { println it }
    }

    @Test
    public void when_split_set_it_returns_map_of_two_sets() {
        Set mySet = [100, 200, 300, 400, 500, 600]
        println mySet.split { it > 300 }
    }

    @Test
    public void when_flatten_set_it_becomes_one_big_flat_set() {
        Set foodSet = [["apple", "banana"], "peas", "green beans", ["egg", "milk", ["cheess", "ice cream"]]]
        assertEquals(4, foodSet.size())

        Set flattenSet = foodSet.flatten()
        assertEquals(8, flattenSet.size())

        println flattenSet
    }

    @Test
    public void when_intersect_set_it_finds_common_elements() {
        Set firstSet = [1, 2, 3, 4, 5]
        Set secondSet = [4, 5, 6, 7, 8]

        Set intersectSet = firstSet.intersect(secondSet)

        assertEquals(2, intersectSet.size())

        def commonNumbers = [4, 5] as Set
        assertTrue(commonNumbers.equals(intersectSet))

        println intersectSet
    }

    @Test
    public void when_removeAll_it_becomes_empty() {
        Set initialSet = [1, 2, 3, 4, 5]
        initialSet.removeAll([1, 2, 3, 4, 5])

        assertTrue(initialSet.size() == 0)
    }

    @Test
    public void demo_addAll_and_unique_list() {
        List names = ["Mary", "Becky", "Susan", "Tom"]

        names.addAll(["Mary", "Susan"])
        assertEquals(6, names.size())

        names.unique();
        assertEquals(4, names.size())

        println names
    }
}
